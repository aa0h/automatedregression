# README #


### What is this repository for? ###

* This is the Automated regression using;
* Selenium IDE (Firefox web automated tests)
* JMeter (Performance tests)

### How do I get set up? ###

* --For the Automated regression--
* Install Firefox. (Download and install version 54.x or earlier, do not download from main Mozilla page as will be newer version)
* Download Selenium IDE plugin (SELENIUM IDE 2.9.1.1-signed)
* From https://addons.mozilla.org/en-US/firefox/addon/selenium-ide/
* Download repositories or sync using SourceTree
* Open Selenium IDE in Firefox 
* Open "IPSM Regression Test Suite.html"
* Run

* --For the Performance Regression--
* Install JMeter. http://jmeter.apache.org/download_jmeter.cgi
* Open JMeter
* Open and Run JMeter scripts

### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin
* Andrew Anderson